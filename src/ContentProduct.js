var Product = require('./Product');

class ContentProduct extends Product
{
    constructor(dataProduct, cart)
    {
        super(dataProduct);
        this.cart = cart;
        this.wrap.className      = 'wrap';
        this.container.className = 'product';
        this.desk       = document.createElement('p');
        this.addCart    = this.addCart.bind(this);
    }

    addCart(){
        this.cart.add(this.data)
    }

    render()
    {
        this.name.innerText = this.data.name;
        this.button.innerText = 'Add to cart';

        this.button.onclick = this.addCart;

        this.image.src = this.data.image;
        this.desk.innerText = this.data.description;

        this.wrap.appendChild(this.name);
        this.wrap.appendChild(this.image);
        this.wrap.appendChild(this.desk);
        this.wrap.appendChild(this.button);
        this.container.appendChild(this.wrap);

        return this.container;
    }
}

module.exports = ContentProduct;