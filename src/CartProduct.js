var Product = require('./Product');

class CartProduct extends Product
{
    constructor(dataProduct, cart)
    {
        super(dataProduct); 
        this.cart = cart;

        this.wrap.className      = 'cart-wrap';
        this.container.className = 'cart-product';
        this.removeCart = this.removeCart.bind(this);
        this.quantity  = document.createElement('span');
    }

    removeCart(){
        this.cart.remove(this.data)
    }

    render()
    {
        this.name.innerText = this.data.name;
        this.button.innerText = 'remove';
        this.quantity.innerText = this.data.quantity;

        this.button.onclick = this.removeCart;

        this.image.src = this.data.image;

        this.wrap.appendChild(this.image);
        this.wrap.appendChild(this.quantity);
        this.wrap.appendChild(this.name);
        
        this.wrap.appendChild(this.button);
        this.container.appendChild(this.wrap);

        return this.container;
    }
}

module.exports = CartProduct;