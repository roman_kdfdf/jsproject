var ContentProduct = require('./ContentProduct');

class BuilderProducts
{
    constructor(data, container_name, cart)
    {
        this.cart = cart;
        this.products = data;
        this.container = document.querySelector(container_name);
        this.show();
    }

    show()
    {
        this.container.innerHTML = "";
        for(var j in this.products)
        {
            var dataProduct = this.products[j];
            var product     = new ContentProduct(dataProduct, this.cart);
            this.container.appendChild(product.render());
        }
    }
}

module.exports = BuilderProducts;