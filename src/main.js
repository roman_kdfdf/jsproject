function c (a){
    console.log(a);
}

var dbProducts = [
    {id:1,  name:'Product-1', image:'prod.jpg', description:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus, sit? Quo dignissimos quos mollitia. Repellendus quam deleniti dolores harum cupiditate assumenda, animi id sit quo cumque explicabo quae temporibus enim!'},
    {id:2,  name:'Product-2', image:'prod.jpg', description:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus, sit? Quo dignissimos quos mollitia. Repellendus quam deleniti dolores harum cupiditate assumenda, animi id sit quo cumque explicabo quae temporibus enim!'},
    {id:3,  name:'Product-3', image:'prod.jpg', description:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus, sit? Quo dignissimos quos mollitia. Repellendus quam deleniti dolores harum cupiditate assumenda, animi id sit quo cumque explicabo quae temporibus enim!'},
    {id:4,  name:'Product-4', image:'prod.jpg', description:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus, sit? Quo dignissimos quos mollitia. Repellendus quam deleniti dolores harum cupiditate assumenda, animi id sit quo cumque explicabo quae temporibus enim!'},
    {id:5,  name:'Product-5', image:'prod.jpg', description:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus, sit? Quo dignissimos quos mollitia. Repellendus quam deleniti dolores harum cupiditate assumenda, animi id sit quo cumque explicabo quae temporibus enim!'},
    {id:6,  name:'Product-6', image:'prod.jpg', description:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus, sit? Quo dignissimos quos mollitia. Repellendus quam deleniti dolores harum cupiditate assumenda, animi id sit quo cumque explicabo quae temporibus enim!'},
    {id:7,  name:'Product-7', image:'prod.jpg', description:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus, sit? Quo dignissimos quos mollitia. Repellendus quam deleniti dolores harum cupiditate assumenda, animi id sit quo cumque explicabo quae temporibus enim!'},
    {id:8,  name:'Product-8', image:'prod.jpg', description:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus, sit? Quo dignissimos quos mollitia. Repellendus quam deleniti dolores harum cupiditate assumenda, animi id sit quo cumque explicabo quae temporibus enim!'},
    {id:9,  name:'Product-9', image:'prod.jpg', description:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus, sit? Quo dignissimos quos mollitia. Repellendus quam deleniti dolores harum cupiditate assumenda, animi id sit quo cumque explicabo quae temporibus enim!'},
];


class Cart
{
    constructor(){
        this.products = [];
        this.container = document.querySelector('#cart');
    }

    add(data)
    {
        console.log('add', data);
        if(!data.quantity || data.quantity === 0){
            data.quantity = 1;
            this.products.push(data);
        }
        else{
            data.quantity ++;
        }
        this.render();
    }

    remove(data)
    {
        data.quantity --;
        
        if(data.quantity === 0)
        {
            this.products = this.products.filter(function(el){
                return el.id !== data.id;
            })
        }
       
        this.render();
    }

    render()
    {
        this.container.innerHTML = "";
        for(var j in this.products){
            var dataProduct = this.products[j];
            var product = new CartProduct(dataProduct);
            this.container.appendChild(product.render());
        }
    }
}


class BuilderProducts
{
    constructor(data, container_name)
    {
        this.products = data;
        this.container = document.querySelector(container_name);
        this.show();
    }

    show()
    {
        this.container.innerHTML = "";
        for(var j in this.products)
        {
            var dataProduct = this.products[j];
            var product     = new ContentProduct(dataProduct);
            this.container.appendChild(product.render());
        }
    }
}

class Product
{
    constructor(dataProduct)
    {
        this.data       = dataProduct;

        this.container  = document.createElement('div');
        this.wrap       = document.createElement('div');
        this.button     = document.createElement('button');
        this.name       = document.createElement('h4');
        this.image      = document.createElement('img');
    }
}


class ContentProduct extends Product
{
    constructor(dataProduct)
    {
        super(dataProduct);

        this.wrap.className      = 'wrap';
        this.container.className = 'product';
        this.desk       = document.createElement('p');
        this.addCart    = this.addCart.bind(this);
    }

    addCart(){
        cart.add(this.data)
    }

    render()
    {
        this.name.innerText = this.data.name;
        this.button.innerText = 'Add to cart';

        this.button.onclick = this.addCart;

        this.image.src = this.data.image;
        this.desk.innerText = this.data.description;

        this.wrap.appendChild(this.name);
        this.wrap.appendChild(this.image);
        this.wrap.appendChild(this.desk);
        this.wrap.appendChild(this.button);
        this.container.appendChild(this.wrap);

        return this.container;
    }
}

class CartProduct extends Product
{
    constructor(dataProduct)
    {
        super(dataProduct); 

        this.wrap.className      = 'cart-wrap';
        this.container.className = 'cart-product';
        this.removeCart = this.removeCart.bind(this);
        this.quantity  = document.createElement('span');
    }

    removeCart(){
        cart.remove(this.data)
    }

    render()
    {
        this.name.innerText = this.data.name;
        this.button.innerText = 'remove';
        this.quantity.innerText = this.data.quantity;

        this.button.onclick = this.removeCart;

        this.image.src = this.data.image;

        this.wrap.appendChild(this.image);
        this.wrap.appendChild(this.quantity);
        this.wrap.appendChild(this.name);
        
        this.wrap.appendChild(this.button);
        this.container.appendChild(this.wrap);

        return this.container;
    }
}

var cart = new Cart();
var builderProducts = new BuilderProducts(dbProducts, '#products-block');
