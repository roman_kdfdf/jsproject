class Product
{
    constructor(dataProduct)
    {
        this.data       = dataProduct;

        this.container  = document.createElement('div');
        this.wrap       = document.createElement('div');
        this.button     = document.createElement('button');
        this.name       = document.createElement('h4');
        this.image      = document.createElement('img');
    }
}

module.exports = Product;