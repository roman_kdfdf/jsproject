var CartProduct = require('./CartProduct');

class Cart
{
    constructor(){
        this.products = [];
        this.container = document.querySelector('#cart');
    }

    add(data)
    {
        console.log('add', data);
        if(!data.quantity || data.quantity === 0){
            data.quantity = 1;
            this.products.push(data);
        }
        else{
            data.quantity ++;
        }
        this.render();
    }

    remove(data)
    {
        data.quantity --;
        
        if(data.quantity === 0)
        {
            this.products = this.products.filter(function(el){
                return el.id !== data.id;
            })
        }
       
        this.render();
    }

    render()
    {
        this.container.innerHTML = "";
        for(var j in this.products){
            var dataProduct = this.products[j];
            var product = new CartProduct(dataProduct, this);
            this.container.appendChild(product.render());
        }
    }
}

module.exports = Cart;